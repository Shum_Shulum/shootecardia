// Shootecardia Game. All Rights Reserved


#include "SC_GameModeBase.h"
#include "AIController.h"
#include "Player/SCBaseCharacter.h"
#include "Player/SCPlayerController.h"
#include "UI/SCGameHUD.h"
#include "Player/SCPlayerState.h"
#include "SCUtils.h"
#include "Components/SCRespawnComponent.h"
#include "EngineUtils.h"
#include "Components/SCWeaponComponent.h"

DEFINE_LOG_CATEGORY_STATIC(GameModeLog, All, All);

constexpr static int32 MinRoundTimeForRespawn = 10;

ASC_GameModeBase::ASC_GameModeBase()
{
	DefaultPawnClass = ASCBaseCharacter::StaticClass();
	PlayerControllerClass = ASCPlayerController::StaticClass();
	HUDClass = ASCGameHUD::StaticClass();
	PlayerStateClass = ASCPlayerState::StaticClass();
}

void ASC_GameModeBase::StartPlay()
{
	Super::StartPlay();

	SpawnBots();

	CreateTeamsInfo();

	CurrentRound = 1;
	StartRound();
	SetMatchState(ESCMatchState::InProgress);
}

UClass* ASC_GameModeBase::GetDefaultPawnClassForController_Implementation(AController* InController)
{
	if (InController && InController->IsA<AAIController>())
	{
		return AIPawnClass;
	}
	return Super::GetDefaultPawnClassForController_Implementation(InController);
}

void ASC_GameModeBase::Killed(AController* KillerController, AController* VictimController)
{
	const auto KillerPlayerState = KillerController ? Cast<ASCPlayerState>(KillerController->PlayerState) : nullptr;
	const auto VictimPlayerState = VictimController ? Cast<ASCPlayerState>(VictimController->PlayerState) : nullptr;

	if (KillerPlayerState)
	{
		KillerPlayerState->AddKill();
	}

	if (VictimPlayerState)
	{
		VictimPlayerState->AddDeath();
	}

	StartRespawn(VictimController);
}

void ASC_GameModeBase::RespawnRequest(AController* Controller)
{
	ResetOnePlayer(Controller);
}

bool ASC_GameModeBase::SetPause(APlayerController* PC, FCanUnpause CanUnpauseDelegate)
{
	const auto PauseSet = Super::SetPause(PC, CanUnpauseDelegate);

	if (PauseSet)
	{
		StopAllFire();
		SetMatchState(ESCMatchState::Pause);
	}

	return PauseSet;
}

bool ASC_GameModeBase::ClearPause()
{
	const auto PauseClear = Super::ClearPause();
	if (PauseClear)
	{
		SetMatchState(ESCMatchState::InProgress);
	}
	return PauseClear;
}

void ASC_GameModeBase::SpawnBots()
{
	if (!GetWorld()) return;

	for (int32 i = 0; i < GameData.PlayersNum - 1; ++i)
	{
		FActorSpawnParameters SpawnInfo;
		SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		const auto SCAIController = GetWorld()->SpawnActor<AAIController>(AIControllerClass, SpawnInfo);
		RestartPlayer(SCAIController);
	}
}

void ASC_GameModeBase::StartRound()
{
	RoundCountDown = GameData.RoundTime;
	GetWorldTimerManager().SetTimer(GameRoundTimerHandle, this, &ASC_GameModeBase::GameTimerUpdate, 1.0f, true);
}

void ASC_GameModeBase::GameTimerUpdate()
{
	UE_LOG(GameModeLog, Display, TEXT("Time: %i / Round %i of %i"), RoundCountDown, CurrentRound, GameData.RoundsNum);
	if (--RoundCountDown == 0)
	{
		GetWorldTimerManager().ClearTimer(GameRoundTimerHandle);

		if (CurrentRound + 1 <= GameData.RoundsNum)
		{
			++CurrentRound;
			ResetPlayers();
			StartRound();
		}
		else
		{
			GameOver();
		}
	}
}

void ASC_GameModeBase::ResetPlayers()
{
	if (!GetWorld()) return;

	for (auto It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		ResetOnePlayer(It->Get());
	}
}

void ASC_GameModeBase::ResetOnePlayer(AController* Controller)
{
	if (Controller && Controller->GetPawn())
	{
		Controller->GetPawn()->Reset();
	}

	RestartPlayer(Controller);

	SetPlayerColor(Controller);
}

void ASC_GameModeBase::CreateTeamsInfo()
{
	if (!GetWorld()) return;

	int32 TeamID = 1;

	for (auto It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		const auto Controller = It->Get();
		if (!Controller) continue;

		const auto PlayerState = Cast<ASCPlayerState>(Controller->PlayerState);
		if (!PlayerState) continue;

		PlayerState->SetTeamID(TeamID);
		PlayerState->SetTeamColor(DetermineColorByTeamID(TeamID));
		PlayerState->SetPlayerName(Controller->IsPlayerController() ? "Player" : "Bot");

		SetPlayerColor(Controller);

		TeamID = TeamID == 1 ? 2 : 1;
	}
}

FLinearColor ASC_GameModeBase::DetermineColorByTeamID(int32 TeamID)
{
	if (TeamID - 1 < GameData.TeamColors.Num())
	{
		return GameData.TeamColors[TeamID - 1];
	}
	UE_LOG(GameModeLog, Warning, TEXT("No color for team id: %i, set to defult: %s"), TeamID,
	       *GameData.DefaultTeamColor.ToString());
	return GameData.DefaultTeamColor;
}

void ASC_GameModeBase::SetPlayerColor(AController* Controller)
{
	if (!Controller) return;

	const auto Character = Cast<ASCBaseCharacter>(Controller->GetPawn());
	if (!Character) return;

	const auto PlayerState = Cast<ASCPlayerState>(Controller->PlayerState);
	if (!PlayerState) return;

	Character->SetPlayerColor(PlayerState->GetTeamColor());
}

void ASC_GameModeBase::LogPlayInfo()
{
	if (!GetWorld()) return;

	for (auto It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		const auto Controller = It->Get();
		if (!Controller) continue;

		const auto PlayerState = Cast<ASCPlayerState>(Controller->PlayerState);
		if (!PlayerState) continue;

		PlayerState->LogInfo();
	}
}

void ASC_GameModeBase::StartRespawn(AController* Controller)
{
	const auto RespawnAvailable = RoundCountDown > MinRoundTimeForRespawn + GameData.RespawnTime;
	if (!RespawnAvailable) return;

	const auto RespawnComponent = SCUtils::GetSCPlayerComponent<USCRespawnComponent>(Controller);
	if (!RespawnComponent) return;

	RespawnComponent->Respawn(GameData.RespawnTime);
}

void ASC_GameModeBase::GameOver()
{
	UE_LOG(GameModeLog, Display, TEXT("=============GAME OVER============="));
	LogPlayInfo();

	for (auto Pawn : TActorRange<APawn>(GetWorld()))
	{
		if (Pawn)
		{
			Pawn->TurnOff();
			Pawn->DisableInput(nullptr);
		}
	}
	SetMatchState(ESCMatchState::GameOver);
}

void ASC_GameModeBase::SetMatchState(ESCMatchState State)
{
	if (MatchState == State) return;

	MatchState = State;
	OnMatchStateChange.Broadcast(MatchState);
}

void ASC_GameModeBase::StopAllFire()
{
	for (auto Pawn : TActorRange<APawn>(GetWorld()))
	{
		const auto WeaponComponent = SCUtils::GetSCPlayerComponent<USCWeaponComponent>(Pawn);
		if (!WeaponComponent) continue;

		WeaponComponent->StopFire();
		WeaponComponent->Zoom(false);
	}
}
