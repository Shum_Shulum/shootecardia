// Shootecardia Game. All Rights Reserved


#include "Pickups/SCBasePickup.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"


ASCBasePickup::ASCBasePickup()
{
	PrimaryActorTick.bCanEverTick = true;
	CollisionComponent = CreateDefaultSubobject<USphereComponent>("SphereComponent");
	CollisionComponent->InitSphereRadius(50.0f);
	CollisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	SetRootComponent(CollisionComponent);
}

void ASCBasePickup::BeginPlay()
{
	Super::BeginPlay();
	GenerateRotationYaw();
}


void ASCBasePickup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	check(CollisionComponent);

	AddActorLocalRotation(FRotator(0.0f, RotationYaw, 0.0f));

	for (const auto OverlapPawn : OverlappingPawns)
	{
		if (GivePickupTo(OverlapPawn))
		{
			PickupWasTaken();
			break;
		}
	}
}

bool ASCBasePickup::CouldBeTaken() const
{
	return !GetWorldTimerManager().IsTimerActive(RespawnTimerHandle);
}

void ASCBasePickup::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	const auto Pawn = Cast<APawn>(OtherActor);
	if (GivePickupTo(Pawn))
	{
		PickupWasTaken();
	}
	else if (Pawn)
	{
		OverlappingPawns.Add(Pawn);
	}
}

void ASCBasePickup::NotifyActorEndOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	const auto Pawn = Cast<APawn>(OtherActor);
	OverlappingPawns.Remove(Pawn);
}

void ASCBasePickup::PickupWasTaken()
{
	CollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);

	if (GetRootComponent())
	{
		GetRootComponent()->SetVisibility(false, true);
	}

	GetWorldTimerManager().SetTimer(RespawnTimerHandle, this, &ASCBasePickup::Respawn, RespawnTime);

	UGameplayStatics::PlaySoundAtLocation(GetWorld(), PickupTakenSound, GetActorLocation());
}

void ASCBasePickup::Respawn()
{
	GenerateRotationYaw();

	if (GetRootComponent())
	{
		GetRootComponent()->SetVisibility(true, true);
	}
	CollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
}

bool ASCBasePickup::GivePickupTo(APawn* PlayerPawn)
{
	return false;
}

void ASCBasePickup::GenerateRotationYaw()
{
	const auto Directon = FMath::RandBool() ? 1.0f : -1.0f;
	RotationYaw = FMath::RandRange(0.2f, 0.5f) * Directon;
}
