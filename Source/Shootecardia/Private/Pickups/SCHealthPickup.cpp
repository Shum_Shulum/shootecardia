// Shootecardia Game. All Rights Reserved


#include "Pickups/SCHealthPickup.h"

#include "SCUtils.h"
#include "Components/SCHealthComponent.h"

DEFINE_LOG_CATEGORY_STATIC(HealthPickupLog, All, All);

bool ASCHealthPickup::GivePickupTo(APawn* PlayerPawn)
{
	const auto HealthComponent = SCUtils::GetSCPlayerComponent<USCHealthComponent>(PlayerPawn);
	if (!HealthComponent) return false;
	
	return HealthComponent->TryToAddHealth(HealthAmount);
}
