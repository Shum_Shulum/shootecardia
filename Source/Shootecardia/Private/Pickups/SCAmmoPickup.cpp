// Shootecardia Game. All Rights Reserved


#include "Pickups/SCAmmoPickup.h"
#include "SCUtils.h"
#include "Components/SCHealthComponent.h"
#include "Components/SCWeaponComponent.h"

DEFINE_LOG_CATEGORY_STATIC(AmmoPickupLog, All, All);

bool ASCAmmoPickup::GivePickupTo(APawn* PlayerPawn)
{
	const auto HealthComponent = SCUtils::GetSCPlayerComponent<USCHealthComponent>(PlayerPawn);
	if (!HealthComponent || HealthComponent->IsDead()) return false;

	const auto WeaponComponent = SCUtils::GetSCPlayerComponent<USCWeaponComponent>(PlayerPawn);
	if (!WeaponComponent) return false;

	return WeaponComponent->TryToAddAmmo(WeaponType, ClipsAmount);
}
