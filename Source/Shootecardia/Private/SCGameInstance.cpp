// Shootecardia Game. All Rights Reserved


#include "SCGameInstance.h"
#include "Sound/SCSoundFuncLib.h"

void USCGameInstance::ToggleVolume()
{
	USCSoundFuncLib::ToggleSoundClassVolume(MasterSoundClass);
}
