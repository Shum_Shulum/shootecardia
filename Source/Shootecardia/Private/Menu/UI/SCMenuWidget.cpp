// Shootecardia Game. All Rights Reserved


#include "Menu/UI/SCMenuWidget.h"
#include "SCGameInstance.h"
#include "Components/Button.h"
#include "Components/HorizontalBox.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Menu/UI/SCLevelItemWidget.h"
#include "Sound/SoundCue.h"


void USCMenuWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	if (StartGameButton)
	{
		StartGameButton->OnClicked.AddDynamic(this, &USCMenuWidget::OnStartGame);
	}

	if (QuitGameButton)
	{
		QuitGameButton->OnClicked.AddDynamic(this, &USCMenuWidget::USCMenuWidget::OnQuitGame);
	}

	InitLevelItems();
}

void USCMenuWidget::OnAnimationFinished_Implementation(const UWidgetAnimation* Animation)
{
	if (Animation != HideAnimation) return;

	const auto SCGameInstance = GetSCGameInstance();
	if (!SCGameInstance) return;

	UGameplayStatics::OpenLevel(this, SCGameInstance->GetStartupLevel().LevelName);
}


void USCMenuWidget::InitLevelItems()
{
	const auto SCGameInstance = GetSCGameInstance();
	if (!SCGameInstance) return;

	checkf(SCGameInstance->GetLevelsData().Num() != 0, TEXT("Levels data must not be empty"));

	if (!LevelItemsBox) return;
	LevelItemsBox->ClearChildren();

	for (auto LevelData : SCGameInstance->GetLevelsData())
	{
		const auto LevelItemWidget = CreateWidget<USCLevelItemWidget>(GetWorld(), LevelItemsWidgetClass);
		if (!LevelItemWidget) continue;

		LevelItemWidget->SetLevelData(LevelData);
		LevelItemWidget->OnLevelSelected.AddUObject(this, &USCMenuWidget::OnLevelSelected);

		LevelItemsBox->AddChild(LevelItemWidget);
		LevelItemWidgets.Add(LevelItemWidget);
	}

	if (SCGameInstance->GetStartupLevel().LevelName.IsNone())
	{
		OnLevelSelected(SCGameInstance->GetLevelsData()[0]);
	}
	else
	{
		OnLevelSelected(SCGameInstance->GetStartupLevel());
	}
}

void USCMenuWidget::OnLevelSelected(const FLevelData& Data)
{
	const auto SCGameInstance = GetSCGameInstance();
	if (!SCGameInstance) return;

	SCGameInstance->SetStartupLevel(Data);

	for (auto LevelItemWidget : LevelItemWidgets)
	{
		if (LevelItemWidget)
		{
			const auto IsSelected = Data.LevelName == LevelItemWidget->GetLevelData().LevelName;
			LevelItemWidget->SetSelected(IsSelected);
		}
	}
}

void USCMenuWidget::OnStartGame()
{
	PlayAnimation(HideAnimation);

	UGameplayStatics::PlaySound2D(GetWorld(), StartGameSound);
}

void USCMenuWidget::OnQuitGame()
{
	UKismetSystemLibrary::QuitGame(this, GetOwningPlayer(), EQuitPreference::Quit, true);
}

USCGameInstance* USCMenuWidget::GetSCGameInstance() const
{
	if (!GetWorld()) return nullptr;
	return GetWorld()->GetGameInstance<USCGameInstance>();
}
