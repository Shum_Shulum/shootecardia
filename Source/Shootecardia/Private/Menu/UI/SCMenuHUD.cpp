// Shootecardia Game. All Rights Reserved


#include "Menu/UI/SCMenuHUD.h"
#include "Blueprint/UserWidget.h"
#include "UI/SCBaseWidget.h"

void ASCMenuHUD::BeginPlay()
{
	Super::BeginPlay();

	if (MenuWidgetClass)
	{
		const auto MenuWidget = CreateWidget<USCBaseWidget>(GetWorld(), MenuWidgetClass);
		if (MenuWidget)
		{
			MenuWidget->AddToViewport();
			MenuWidget->Show();
		}
	}
}
