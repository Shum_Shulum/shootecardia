// Shootecardia Game. All Rights Reserved


#include "Menu/SCMenuPlayerController.h"

#include "SCGameInstance.h"

void ASCMenuPlayerController::BeginPlay()
{
	Super::BeginPlay();

	SetInputMode(FInputModeUIOnly());

	bShowMouseCursor = true;
}
