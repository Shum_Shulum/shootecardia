// Shootecardia Game. All Rights Reserved


#include "Menu/SCMenuGameModeBase.h"

#include "Menu/SCMenuPlayerController.h"
#include "Menu/UI/SCMenuHUD.h"

ASCMenuGameModeBase::ASCMenuGameModeBase()
{
	PlayerControllerClass=ASCMenuPlayerController::StaticClass();
	HUDClass=ASCMenuHUD::StaticClass();
}
