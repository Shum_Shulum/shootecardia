// Shootecardia Game. All Rights Reserved


#include "AI/Services/SCChangeWeaponService.h"
#include "AIController.h"
#include "SCUtils.h"
#include "Components/SCWeaponComponent.h"

USCChangeWeaponService::USCChangeWeaponService()
{
	NodeName = "Change Weapon";
}

void USCChangeWeaponService::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	const auto Controller = OwnerComp.GetAIOwner();

	if (Controller)
	{
		const auto WeaponComponent = SCUtils::GetSCPlayerComponent<USCWeaponComponent>(Controller->GetPawn());
		if (WeaponComponent && Probability > 0 && FMath::FRand() <= Probability)
		{
			WeaponComponent->NextWeapon();
		}
	}

	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
}
