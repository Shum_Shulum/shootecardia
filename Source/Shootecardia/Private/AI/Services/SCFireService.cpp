// Shootecardia Game. All Rights Reserved


#include "AI/Services/SCFireService.h"
#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "SCUtils.h"
#include "Components/SCWeaponComponent.h"

USCFireService::USCFireService()
{
	NodeName = "Fire";
}

void USCFireService::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	const auto Controller = OwnerComp.GetAIOwner();
	const auto Blackboard = OwnerComp.GetBlackboardComponent();

	const auto HasAim = Blackboard && Blackboard->GetValueAsObject(EnemyActorKey.SelectedKeyName);

	if (Controller)
	{
		const auto WeaponComponent = SCUtils::GetSCPlayerComponent<USCWeaponComponent>(Controller->GetPawn());
		if (WeaponComponent)
		{
			HasAim ? WeaponComponent->StartFire() : WeaponComponent->StopFire();
		}
	}
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
}
