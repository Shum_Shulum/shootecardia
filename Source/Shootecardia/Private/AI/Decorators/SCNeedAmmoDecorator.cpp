// Shootecardia Game. All Rights Reserved


#include "AI/Decorators/SCNeedAmmoDecorator.h"
#include "AIController.h"
#include "SCUtils.h"
#include "Components/SCWeaponComponent.h"

USCNeedAmmoDecorator::USCNeedAmmoDecorator()
{
	NodeName = "Need Ammo";
}

bool USCNeedAmmoDecorator::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
	const auto Controller = OwnerComp.GetAIOwner();
	if (!Controller) return false;

	const auto WeaponComponent = SCUtils::GetSCPlayerComponent<USCWeaponComponent>(Controller->GetPawn());
	if (!WeaponComponent) return false;

	return WeaponComponent->NeedAmmo(WeaponType);
}
