// Shootecardia Game. All Rights Reserved


#include "AI/Decorators/SCHelthPercentDecorator.h"
#include "AIController.h"
#include "SCUtils.h"
#include "Components/SCHealthComponent.h"

USCHelthPercentDecorator::USCHelthPercentDecorator()
{
	NodeName = "Health Percent";
}

bool USCHelthPercentDecorator::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
	const auto Controller = OwnerComp.GetAIOwner();
	if (!Controller) return false;

	const auto HealthComponent = SCUtils::GetSCPlayerComponent<USCHealthComponent>(Controller->GetPawn());
	if (!HealthComponent || HealthComponent->IsDead()) return false;

	return HealthComponent->GetHealthPercent() <= HealthPercent;
}
