// Shootecardia Game. All Rights Reserved


#include "AI/SCAIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AI/SCAICharacter.h"
#include "Components/SCAIPerceptionComponent.h"
#include "Components/SCRespawnComponent.h"

ASCAIController::ASCAIController()
{
	SCAIPerceptionComponent = CreateDefaultSubobject<USCAIPerceptionComponent>("SCAIPerceptionComponent");
	SetPerceptionComponent(*SCAIPerceptionComponent);
	RespawnComponent = CreateDefaultSubobject<USCRespawnComponent>("RespawnComponent");

	bWantsPlayerState = true;
}

void ASCAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	const auto SCCharacter = Cast<ASCAICharacter>(InPawn);
	if (SCCharacter)
	{
		RunBehaviorTree(SCCharacter->BehaviorTreeAsset);
	}
}

void ASCAIController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	const auto AimActor = GetFocusOnActor();
	SetFocus(AimActor);
}

AActor* ASCAIController::GetFocusOnActor() const
{
	if (!GetBlackboardComponent()) return nullptr;
	return Cast<AActor>(GetBlackboardComponent()->GetValueAsObject(FocusOnKeyName));
}
