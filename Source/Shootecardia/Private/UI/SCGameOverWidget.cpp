// Shootecardia Game. All Rights Reserved


#include "UI/SCGameOverWidget.h"
#include "SC_GameModeBase.h"
#include "Player/SCPlayerState.h"
#include "UI/SCPlayerStateRawWidget.h"
#include "Components/VerticalBox.h"
#include "SCUtils.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"

void USCGameOverWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	if (GetWorld())
	{
		const auto GameMode = Cast<ASC_GameModeBase>(GetWorld()->GetAuthGameMode());
		if (GameMode)
		{
			GameMode->OnMatchStateChange.AddUObject(this, &USCGameOverWidget::OnMatchStateChange);
		}
	}

	if (ResetLevelButton)
	{
		ResetLevelButton->OnClicked.AddDynamic(this, &USCGameOverWidget::USCGameOverWidget::OnResetLevel);
	}
}

void USCGameOverWidget::OnMatchStateChange(ESCMatchState State)
{
	if (State == ESCMatchState::GameOver)
	{
		UpdatePlayerState();
	}
}

void USCGameOverWidget::UpdatePlayerState()
{
	if (!GetWorld() || !PlayerStatBox) return;

	PlayerStatBox->ClearChildren();

	for (auto It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		const auto Controller = It->Get();
		if (!Controller) continue;

		const auto PlayerState = Cast<ASCPlayerState>(Controller->PlayerState);
		if (!PlayerState) continue;

		const auto PlayerStatRowWidget = CreateWidget<USCPlayerStateRawWidget>(GetWorld(), PlayerStatRowWidgetClass);
		if (!PlayerStatRowWidget) continue;

		PlayerStatRowWidget->SetPlayerName(FText::FromString(PlayerState->GetPlayerName()));

		PlayerStatRowWidget->SetKills(SCUtils::TextFromInt(PlayerState->GetKillsNum()));
		PlayerStatRowWidget->SetDeaths(SCUtils::TextFromInt(PlayerState->GetDeathsNum()));
		PlayerStatRowWidget->SetTeam(SCUtils::TextFromInt(PlayerState->GetTeamID()));
		PlayerStatRowWidget->SetPlayerIndicatorVisibility(Controller->IsPlayerController());
		PlayerStatRowWidget->SetTeamColor(PlayerState->GetTeamColor());

		PlayerStatBox->AddChild(PlayerStatRowWidget);
	}
}

void USCGameOverWidget::OnResetLevel()
{
	const FString CurrentLevelName = UGameplayStatics::GetCurrentLevelName(this);
	UGameplayStatics::OpenLevel(GetWorld(), FName(CurrentLevelName));
}
