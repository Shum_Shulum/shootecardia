// Shootecardia Game. All Rights Reserved


#include "UI/SCGameDataWidget.h"
#include "Player/SCPlayerState.h"
#include "SC_GameModeBase.h"

int32 USCGameDataWidget::GetCurrentRoundNum() const
{
	const auto GameMode = GetSCGameModeBase();
	return GameMode ? GameMode->GetCurrentRoundNum() : 0;
}

int32 USCGameDataWidget::GetTotalRoundsNum() const
{
	const auto GameMode = GetSCGameModeBase();
	return GameMode ? GameMode->GetGameData().RoundsNum : 0;
}

int32 USCGameDataWidget::GetRoundSecondsRemaining() const
{
	const auto GameMode = GetSCGameModeBase();
	return GameMode ? GameMode->GetRoundSecondsRemaining() : 0;
}

ASC_GameModeBase* USCGameDataWidget::GetSCGameModeBase() const
{
	return GetWorld() ? Cast<ASC_GameModeBase>(GetWorld()->GetAuthGameMode()) : nullptr;
}

ASCPlayerState* USCGameDataWidget::GetSCPlayerState() const
{
	return GetOwningPlayer() ? Cast<ASCPlayerState>(GetOwningPlayer()->PlayerState) : nullptr;
}
