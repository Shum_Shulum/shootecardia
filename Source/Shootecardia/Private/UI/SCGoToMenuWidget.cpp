// Shootecardia Game. All Rights Reserved


#include "UI/SCGoToMenuWidget.h"
#include "SCGameInstance.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"

DEFINE_LOG_CATEGORY_STATIC(LogSCGoToMenuWidget, All, All);

void USCGoToMenuWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	if (GoToMenuButton)
	{
		GoToMenuButton->OnClicked.AddDynamic(this, &USCGoToMenuWidget::OnGoToMenu);
	}
}

void USCGoToMenuWidget::OnGoToMenu()
{
	if (!GetWorld()) return;

	const auto SCGameInstance = GetWorld()->GetGameInstance<USCGameInstance>();
	if (!SCGameInstance) return;

	if (SCGameInstance->GetMenuLevelName().IsNone())
	{
		UE_LOG (LogSCGoToMenuWidget, Error, TEXT("Name is NONE!"));
		return;
	}

	UGameplayStatics::OpenLevel(this, SCGameInstance->GetMenuLevelName());
	
}
