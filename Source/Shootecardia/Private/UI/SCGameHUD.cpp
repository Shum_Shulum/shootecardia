// Shootecardia Game. All Rights Reserved


#include "UI/SCGameHUD.h"
#include "SC_GameModeBase.h"
#include "UI/SCBaseWidget.h"
#include "Engine/Canvas.h"

DEFINE_LOG_CATEGORY_STATIC(GameHUDLog, All, All);

void ASCGameHUD::DrawHUD()
{
	Super::DrawHUD();
}

void ASCGameHUD::BeginPlay()
{
	Super::BeginPlay();

	GameWidgets.Add(ESCMatchState::InProgress, CreateWidget<USCBaseWidget>(GetWorld(), PlayerHUDWidgetClass));
	GameWidgets.Add(ESCMatchState::Pause, CreateWidget<USCBaseWidget>(GetWorld(), PauseWidgetClass));
	GameWidgets.Add(ESCMatchState::GameOver, CreateWidget<USCBaseWidget>(GetWorld(), GameOverWidgetClass));

	for (auto GameWidgetPair : GameWidgets)
	{
		const auto GameWidget = GameWidgetPair.Value;
		if (!GameWidget) continue;

		GameWidget->AddToViewport();
		GameWidget->SetVisibility(ESlateVisibility::Hidden);
	}
	if (GetWorld())
	{
		const auto GameMode = Cast<ASC_GameModeBase>(GetWorld()->GetAuthGameMode());
		if (GameMode)
		{
			GameMode->OnMatchStateChange.AddUObject(this, &ASCGameHUD::OnMatchStateChanged);
		}
	}
}

void ASCGameHUD::OnMatchStateChanged(ESCMatchState State)
{
	if (CurrentWidget)
	{
		CurrentWidget->SetVisibility(ESlateVisibility::Hidden);
	}

	if (GameWidgets.Contains(State))
	{
		CurrentWidget = GameWidgets[State];
	}

	if (CurrentWidget)
	{
		CurrentWidget->SetVisibility(ESlateVisibility::Visible);
		CurrentWidget->Show();
	}

	UE_LOG(GameHUDLog, Display, TEXT("Match state changed: %s"), *UEnum::GetValueAsString(State));
}

void ASCGameHUD::DrowCrossHair()
{
	const TInterval<float> Center(Canvas->SizeX * 0.5f, Canvas->SizeY * 0.5f);

	const float HalfLineSize = 10.0f;

	const float LineThickness = 2.0f;

	const FLinearColor LineColor = FLinearColor::Green;

	DrawLine(Center.Min - HalfLineSize, Center.Max, Center.Min + HalfLineSize, Center.Max, LineColor, LineThickness);
	DrawLine(Center.Min, Center.Max - HalfLineSize, Center.Min, Center.Max + HalfLineSize, LineColor, LineThickness);
}
