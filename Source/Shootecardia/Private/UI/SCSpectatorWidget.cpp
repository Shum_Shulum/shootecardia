// Shootecardia Game. All Rights Reserved


#include "UI/SCSpectatorWidget.h"
#include "SCUtils.h"
#include "Components/SCRespawnComponent.h"

bool USCSpectatorWidget::GetRespawnTime(int32& CountDownTime) const
{
	const auto RespawnComponent = SCUtils::GetSCPlayerComponent<USCRespawnComponent>(GetOwningPlayer());
	if (!RespawnComponent || !RespawnComponent->IsRespawnInProgress()) return false;

	CountDownTime = RespawnComponent->GetRespawnCountDown();
	return true;
}
