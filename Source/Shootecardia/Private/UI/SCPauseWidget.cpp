// Shootecardia Game. All Rights Reserved


#include "UI/SCPauseWidget.h"
#include "GameFramework/GameModeBase.h"
#include "Components/Button.h"


void USCPauseWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	if (ClearPauseButton)
	{
		ClearPauseButton->OnClicked.AddDynamic(this, &USCPauseWidget::OnClearPause);
	}
}

void USCPauseWidget::OnClearPause()
{
	if (!GetWorld() || !GetWorld()->GetAuthGameMode()) return;

	GetWorld()->GetAuthGameMode()->ClearPause();
}
