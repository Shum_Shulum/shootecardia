// Shootecardia Game. All Rights Reserved


#include "UI/SCHealthBarWidget.h"
#include "Components/ProgressBar.h"

void USCHealthBarWidget::SetHealthPercent(float Percent)
{
	if (!HealthProgressBar) return;

	const auto HealthBarVisibility = (Percent > PercentVisivilityThreshold || FMath::IsNearlyZero(Percent)
		                                  ? ESlateVisibility::Hidden
		                                  : ESlateVisibility::Visible);
	HealthProgressBar->SetVisibility(HealthBarVisibility);

	const auto HealthBarColor = Percent > PercentColorThreshold ? GoodColor : BadColor;
	HealthProgressBar->SetFillColorAndOpacity(HealthBarColor);

	HealthProgressBar->SetPercent(Percent);
}
