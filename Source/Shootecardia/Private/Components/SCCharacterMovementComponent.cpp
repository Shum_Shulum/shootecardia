// Shootecardia Game. All Rights Reserved


#include "Components/SCCharacterMovementComponent.h"
#include "Player/SCBaseCharacter.h"

float USCCharacterMovementComponent::GetMaxSpeed() const
{
	const float MaxSpeed = Super::GetMaxSpeed();
	const ASCBaseCharacter* Player = Cast<ASCBaseCharacter>(GetPawnOwner());
	return Player && Player->IsRunning() ? MaxSpeed * RunModifier : MaxSpeed;
}
