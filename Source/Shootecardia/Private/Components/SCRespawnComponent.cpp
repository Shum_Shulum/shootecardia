// Shootecardia Game. All Rights Reserved


#include "Components/SCRespawnComponent.h"
#include "SC_GameModeBase.h"

USCRespawnComponent::USCRespawnComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void USCRespawnComponent::Respawn(int32 RespawnTime)
{
	if (!GetWorld()) return;

	RespawnCountDown = RespawnTime;
	GetWorld()->GetTimerManager().SetTimer(RespawnTimerHandle, this, &USCRespawnComponent::RespawnTimerUpdate, 1.0f,
	                                       true);
}

bool USCRespawnComponent::IsRespawnInProgress() const
{
	return GetWorld() && GetWorld()->GetTimerManager().IsTimerActive(RespawnTimerHandle);
}

void USCRespawnComponent::RespawnTimerUpdate()
{
	if (--RespawnCountDown == 0)
	{
		if (!GetWorld()) return;
		GetWorld()->GetTimerManager().ClearTimer(RespawnTimerHandle);

		const auto GameMode = Cast<ASC_GameModeBase>(GetWorld()->GetAuthGameMode());
		if (!GameMode) return;

		GameMode->RespawnRequest(Cast<AController>(GetOwner()));
	}
}
