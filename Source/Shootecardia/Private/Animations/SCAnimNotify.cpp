// Shootecardia Game. All Rights Reserved


#include "Animations/SCAnimNotify.h"

void USCAnimNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	OnNotified.Broadcast(MeshComp);
	Super::Notify(MeshComp, Animation);
}