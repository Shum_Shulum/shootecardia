// Shootecardia Game. All Rights Reserved


#include "Weapon/Components/SCWeaponFXComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "Components/DecalComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"


USCWeaponFXComponent::USCWeaponFXComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void USCWeaponFXComponent::PlayImpactFX(const FHitResult& Hit)
{
	auto ImpactData = DefaultImpactData;

	// Hit.PhysMaterial - хранит информацию о физическом материале в который мы попали
	if (Hit.PhysMaterial.IsValid())
	{
		const auto PhysMat = Hit.PhysMaterial.Get();
		if (ImpactDataMap.Contains(PhysMat))
		// Проверяем, существует ли в нашем контейнере данный физический материал в который мы попали
		{
			ImpactData = ImpactDataMap[PhysMat];
			// добавляем настройки в BaseWeapon и Projectile
		}
	}
	UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), ImpactData.NiagaraEffect, Hit.ImpactPoint,
	                                               Hit.ImpactNormal.Rotation());
	// SpawnSystemAtLocation - спаунит ниагара систему в мир
	// 1 - указатель на мир
	// 2 - указатель на Ниагара систему
	// 3 - локация в мир
	// 4 - ориентация в пространстве

	// спаун декаля
	auto DecalComponent = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), ImpactData.DecalData.Material,
	                                                             ImpactData.DecalData.Size,
	                                                             Hit.ImpactPoint, Hit.ImpactNormal.Rotation());
	// 1 - указатель на мир
	// 2 - материал декали
	// 3 - размер декаля
	// 4 - локация
	// 5 - вращение декаля

	// назначаем параметры декаля
	if (DecalComponent)
	{
		DecalComponent->SetFadeOut(ImpactData.DecalData.LifeTime, ImpactData.DecalData.FadeOutTime);
	}

	// sound
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), ImpactData.Sound, Hit.ImpactPoint);
}
