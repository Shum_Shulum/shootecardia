// Shootecardia Game. All Rights Reserved


#include "Player/SCBaseCharacter.h"
#include "Components/CapsuleComponent.h"
#include "Components/SCCharacterMovementComponent.h"
#include "Components/SCHealthComponent.h"
#include "Components/SCWeaponComponent.h"
#include "Components/TextRenderComponent.h"
#include "Sound/SoundCue.h"
#include "Kismet/GameplayStatics.h"


DEFINE_LOG_CATEGORY_STATIC(SCBaseCharacterLog, All, All);

ASCBaseCharacter::ASCBaseCharacter(const FObjectInitializer& ObjInit)
	: Super(ObjInit.SetDefaultSubobjectClass<USCCharacterMovementComponent>(ACharacter::CharacterMovementComponentName))
{
	PrimaryActorTick.bCanEverTick = true;

	HealthComponent = CreateDefaultSubobject<USCHealthComponent>("HealthComponent");

	WeaponComponent = CreateDefaultSubobject<USCWeaponComponent>("WeaponComponent");
}


void ASCBaseCharacter::BeginPlay()
{
	Super::BeginPlay();

	check(HealthComponent);
	check(GetCharacterMovement());

	HealthComponent->OnDeath.AddUObject(this, &ASCBaseCharacter::OnDeath);
	HealthComponent->OnHealthChanged.AddUObject(this, &ASCBaseCharacter::OnHealthChanged);
	OnHealthChanged(HealthComponent->GetHealth(), 0.0f);
	LandedDelegate.AddDynamic(this, &ASCBaseCharacter::OnGroundLanded);
}

void ASCBaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ASCBaseCharacter::TurnOff()
{
	WeaponComponent->StopFire();
	WeaponComponent->Zoom(false);
	Super::TurnOff();
}

void ASCBaseCharacter::Reset()
{
	WeaponComponent->StopFire();
	WeaponComponent->Zoom(false);
	Super::Reset();
}


bool ASCBaseCharacter::IsRunning() const
{
	return false;
}

float ASCBaseCharacter::GetMovementDirection() const
{
	if (GetVelocity().IsZero()) return 0.0f;

	const auto VelocityNormal = GetVelocity().GetSafeNormal();

	const auto AngleBetween = FMath::Acos(FVector::DotProduct(GetActorForwardVector(), VelocityNormal));

	const auto CrossProduct = FVector::CrossProduct(GetActorForwardVector(), VelocityNormal);

	const auto Degrees = FMath::RadiansToDegrees(AngleBetween);

	return CrossProduct.IsZero() ? Degrees : Degrees * FMath::Sign(CrossProduct.Z);
}

void ASCBaseCharacter::SetPlayerColor(const FLinearColor& Color)
{
	const auto MaterialInst = GetMesh()->CreateAndSetMaterialInstanceDynamic(0);
	if (!MaterialInst) return;

	MaterialInst->SetVectorParameterValue(MaterialColorName, Color);
}


void ASCBaseCharacter::OnDeath()
{
	UE_LOG(SCBaseCharacterLog, Display, TEXT("Character %s is dead"), *GetName());

	GetCharacterMovement()->DisableMovement();
	SetLifeSpan(LifeSpanOnDeath);

	GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);

	WeaponComponent->StopFire();
	WeaponComponent->Zoom(false);

	GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	GetMesh()->SetSimulatePhysics(true);

	UGameplayStatics::PlaySoundAtLocation(GetWorld(), DeathSound, GetActorLocation());
}

void ASCBaseCharacter::OnHealthChanged(float Health, float HealthDelta)
{
}

void ASCBaseCharacter::OnGroundLanded(const FHitResult& Hit)
{
	const auto FallVelocityZ = -GetVelocity().Z;

	if (FallVelocityZ < LandedDamageVelocity.X) return;

	const auto FallDamage = FMath::GetMappedRangeValueClamped(LandedDamageVelocity, LandedDamage, FallVelocityZ);

	TakeDamage(FallDamage, FPointDamageEvent{}, nullptr, nullptr);
}
