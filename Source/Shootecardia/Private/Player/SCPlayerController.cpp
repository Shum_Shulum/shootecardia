// Shootecardia Game. All Rights Reserved


#include "Player/SCPlayerController.h"
#include "SC_GameModeBase.h"
#include "Components/SCRespawnComponent.h"
#include "SCGameInstance.h"

ASCPlayerController::ASCPlayerController()
{
	RespawnComponent = CreateDefaultSubobject<USCRespawnComponent>("RespawnComponent");
}

void ASCPlayerController::BeginPlay()
{
	Super::BeginPlay();

	if (GetWorld())
	{
		const auto GameMode = Cast<ASC_GameModeBase>(GetWorld()->GetAuthGameMode());
		if (GameMode)
		{
			GameMode->OnMatchStateChange.AddUObject(this, &ASCPlayerController::OnMatchStateChange);
		}
	}
}

void ASCPlayerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	OnNewPawn.Broadcast(InPawn);
}

void ASCPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	if (!InputComponent) return;

	InputComponent->BindAction("PauseGame", IE_Pressed, this, &ASCPlayerController::OnPauseGame);
	InputComponent->BindAction("Mute", IE_Pressed, this, &ASCPlayerController::OnMuteSound);
}

void ASCPlayerController::OnPauseGame()
{
	if (!GetWorld() || !GetWorld()->GetAuthGameMode()) return;

	GetWorld()->GetAuthGameMode()->SetPause(this);
}

void ASCPlayerController::OnMatchStateChange(ESCMatchState State)
{
	if (State == ESCMatchState::InProgress)
	{
		SetInputMode(FInputModeGameOnly());
		bShowMouseCursor = false;
	}
	else
	{
		SetInputMode(FInputModeUIOnly());
		bShowMouseCursor = true;
	}
}

void ASCPlayerController::OnMuteSound()
{
	if (!GetWorld()) return;
	const auto GameInstance = Cast<USCGameInstance>(GetWorld()->GetGameInstance());
	if (!GameInstance) return;

	GameInstance->ToggleVolume();
}
