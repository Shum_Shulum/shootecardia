// Shootecardia Game. All Rights Reserved


#include "Player/SCPlayerState.h"

DEFINE_LOG_CATEGORY_STATIC(PlayerStateLog, All, All);

void ASCPlayerState::LogInfo()
{
	UE_LOG(PlayerStateLog, Display, TEXT("TeamID: %i / Kills: %i / Deaths: %i"), TeamID, KillsNum, DeathsNum);
}
