// Shootecardia Game. All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "SCCoreTypes.h"
#include "SCGameInstance.generated.h"

class USoundClass;

UCLASS()
class SHOOTECARDIA_API USCGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	FLevelData GetStartupLevel() const { return StartupLevel; }
	void SetStartupLevel(const FLevelData& Data) { StartupLevel = Data; }

	TArray<FLevelData> GetLevelsData() const { return LevelsData; }
	FName GetMenuLevelName() const { return MenuLevelName; }

	void ToggleVolume();

protected:
	UPROPERTY(EditDefaultsOnly, Category="Sound")
	USoundClass* MasterSoundClass;

	UPROPERTY(EditDefaultsOnly, Category="Game")
	TArray<FLevelData> LevelsData;

	UPROPERTY(EditDefaultsOnly, Category="Game", meta = (ToolTip = "Levels names must be unique"))
	FName MenuLevelName = NAME_None;

private:
	UPROPERTY(EditDefaultsOnly, Category="Game")
	FLevelData StartupLevel;
};
