// Shootecardia Game. All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SCSpectatorWidget.generated.h"

UCLASS()
class SHOOTECARDIA_API USCSpectatorWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category="UI")
	bool GetRespawnTime(int32& CountDownTime) const;
};
