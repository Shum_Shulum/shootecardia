// Shootecardia Game. All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SCBaseWidget.generated.h"

class USoundCue;

UCLASS()
class SHOOTECARDIA_API USCBaseWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	void Show();

protected:
	UPROPERTY(meta=(BindWidgetAnim), Transient)
	UWidgetAnimation* ShowAnimation;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite,Category="Sound")
	USoundCue* OpenSound;

	
	
};
