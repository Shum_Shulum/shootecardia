// Shootecardia Game. All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "SCCoreTypes.h"
#include "SCGameHUD.generated.h"

class USCBaseWidget;

UCLASS()
class SHOOTECARDIA_API ASCGameHUD : public AHUD
{
	GENERATED_BODY()

public:
	virtual void DrawHUD() override;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="UI")
	TSubclassOf<UUserWidget> PlayerHUDWidgetClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="UI")
	TSubclassOf<UUserWidget> PauseWidgetClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="UI")
	TSubclassOf<UUserWidget> GameOverWidgetClass;

	virtual void BeginPlay() override;

private:
	UPROPERTY()
	TMap<ESCMatchState, USCBaseWidget*> GameWidgets;

	UPROPERTY()
	USCBaseWidget* CurrentWidget = nullptr;

	void DrowCrossHair();

	void OnMatchStateChanged(ESCMatchState State);
};
