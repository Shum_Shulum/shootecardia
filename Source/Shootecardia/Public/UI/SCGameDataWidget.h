// Shootecardia Game. All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SCGameDataWidget.generated.h"


class ASC_GameModeBase;
class ASCPlayerState;

UCLASS()
class SHOOTECARDIA_API USCGameDataWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category="UI")
	int32 GetCurrentRoundNum() const;

	UFUNCTION(BlueprintCallable, Category="UI")
	int32 GetTotalRoundsNum() const;

	UFUNCTION(BlueprintCallable, Category="UI")
	int32 GetRoundSecondsRemaining() const;

private:
	UFUNCTION(BlueprintCallable, Category="UI")
	ASC_GameModeBase* GetSCGameModeBase() const;

	UFUNCTION(BlueprintCallable, Category="UI")
	ASCPlayerState* GetSCPlayerState() const;
};
