// Shootecardia Game. All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SCCoreTypes.h"
#include "UI/SCBaseWidget.h"
#include "SCGameOverWidget.generated.h"

class UVerticalBox;
class UButton;

UCLASS()
class SHOOTECARDIA_API USCGameOverWidget : public USCBaseWidget
{
	GENERATED_BODY()

protected:
	UPROPERTY(meta=(BindWidget))
	UVerticalBox* PlayerStatBox;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="UI")
	TSubclassOf<UUserWidget> PlayerStatRowWidgetClass;

	UPROPERTY(meta=(BindWidget))
	UButton* ResetLevelButton;

	virtual void NativeOnInitialized() override;

private:
	void OnMatchStateChange(ESCMatchState State);

	void UpdatePlayerState();

	UFUNCTION()
	void OnResetLevel();
};
