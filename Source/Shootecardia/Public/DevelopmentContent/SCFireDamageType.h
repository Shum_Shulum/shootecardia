// Shootecardia Game. All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"
#include "SCFireDamageType.generated.h"

UCLASS()
class SHOOTECARDIA_API USCFireDamageType : public UDamageType
{
	GENERATED_BODY()
	
};
