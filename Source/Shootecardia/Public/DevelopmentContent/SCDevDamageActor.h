// Shootecardia Game. All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SCDevDamageActor.generated.h"

class USceneComponent;

UCLASS()
class SHOOTECARDIA_API ASCDevDamageActor : public AActor
{
	GENERATED_BODY()
	
public:	
	ASCDevDamageActor();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category="Component")
	USceneComponent* SceneComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DebugSphere")
	float Radius = 300.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DebugSphere")
	FColor Color = FColor::Red;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Damage")
	float Damage = 10.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Damage")
	bool DoFullDamage = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Damage")
	TSubclassOf<UDamageType> DamageType;
	
protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

};
