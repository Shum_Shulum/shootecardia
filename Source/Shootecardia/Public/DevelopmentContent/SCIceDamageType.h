// Shootecardia Game. All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"
#include "SCIceDamageType.generated.h"

UCLASS()
class SHOOTECARDIA_API USCIceDamageType : public UDamageType
{
	GENERATED_BODY()
	
};
