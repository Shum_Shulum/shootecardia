// Shootecardia Game. All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "Animations/SCAnimNotify.h"
#include "SCReloadFinishedAnimNotify.generated.h"

UCLASS()
class SHOOTECARDIA_API USCReloadFinishedAnimNotify : public USCAnimNotify
{
	GENERATED_BODY()
	
};
