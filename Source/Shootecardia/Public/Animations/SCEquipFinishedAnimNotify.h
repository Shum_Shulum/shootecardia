// Shootecardia Game. All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "SCAnimNotify.h"
#include "SCEquipFinishedAnimNotify.generated.h"

UCLASS()
class SHOOTECARDIA_API USCEquipFinishedAnimNotify : public USCAnimNotify
{
	GENERATED_BODY()

};
