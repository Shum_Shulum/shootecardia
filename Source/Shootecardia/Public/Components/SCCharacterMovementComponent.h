// Shootecardia Game. All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "SCCharacterMovementComponent.generated.h"

UCLASS()
class SHOOTECARDIA_API USCCharacterMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Movement", meta = (ClampMin="1.2", ClampMax="3.0"))
	float RunModifier = 2.0f;

	virtual float GetMaxSpeed() const override;
};
