// Shootecardia Game. All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SCCoreTypes.h"
#include "Weapon/SCBaseWeapon.h"
#include "SCWeaponComponent.generated.h"

class ASCBaseWeapon;
class USceneComponent;
class UAnimMontage;
class UAnimSequenceBase;

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class SHOOTECARDIA_API USCWeaponComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	USCWeaponComponent();

	virtual void StartFire();
	void StopFire();

	virtual void NextWeapon();

	void Reload();

	bool GetCurrentWeaponUIData(FWeaponUIData& UIData) const;

	bool GetCurrentWeaponAmmoData(FAmmoData& AmmoData) const;

	bool TryToAddAmmo(TSubclassOf<ASCBaseWeapon> WeaponType, int32 ClipsAmount);

	bool NeedAmmo(TSubclassOf<ASCBaseWeapon> WeaponType);

	bool IsFiring() const;

	void Zoom(bool Enabled);

protected:
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UPROPERTY(EditDefaultsOnly, Category="Weapon")
	TArray<FWeaponData> WeaponData;

	UPROPERTY(EditDefaultsOnly, Category="Weapon")
	FName WeaponEquipSocketName = "WeaponSocket";

	UPROPERTY(EditDefaultsOnly, Category="Weapon")
	FName WeaponArmorySocketName = "ArmorySocket";

	UPROPERTY(EditDefaultsOnly, Category="Animation")
	UAnimMontage* EquipAnimMontage;

	UPROPERTY()
	ASCBaseWeapon* CurrentWeapon = nullptr;

	UPROPERTY()
	TArray<ASCBaseWeapon*> Weapons;

	bool CanFire() const;

	bool CanEquip() const;

	void EquipWeapon(int32 WeaponIndex);

	int32 CurrentWeaponIndex = 0;

private:
	UPROPERTY()
	UAnimMontage* CurrentReloadAnimMontage = nullptr;

	bool EquipAnimInProgress = false;

	bool ReloadAnimInProgress = false;

	void SpawnWeapons();

	void AttachWeaponToSocket(ASCBaseWeapon* Weapon, USceneComponent* SceneComponent, const FName& SocketName);

	void PlayAnimMontage(UAnimMontage* Animation);

	void InitAnimations();

	void OnEquipFinished(USkeletalMeshComponent* MeshComp);

	void OnReloadFinished(USkeletalMeshComponent* MeshComp);

	bool CanReload() const;

	void OnClipEmpty(ASCBaseWeapon* AmmoEmptyWeapon);

	void ChangeClip();
};
