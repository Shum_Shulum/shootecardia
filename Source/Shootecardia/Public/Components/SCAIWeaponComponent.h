// Shootecardia Game. All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/SCWeaponComponent.h"
#include "SCAIWeaponComponent.generated.h"

UCLASS()
class SHOOTECARDIA_API USCAIWeaponComponent : public USCWeaponComponent
{
	GENERATED_BODY()

public:
	virtual void StartFire() override;
	virtual void NextWeapon() override;
	
	
};
