// Shootecardia Game. All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "SCCoreTypes.h"
#include "GameFramework/PlayerController.h"
#include "SCPlayerController.generated.h"

class USCRespawnComponent;
UCLASS()
class SHOOTECARDIA_API ASCPlayerController : public APlayerController
{
	GENERATED_BODY()
public:
	ASCPlayerController();
	
protected:
	UPROPERTY(VisibleAnywhere,BlueprintReadWrite,Category="Components")
	USCRespawnComponent* RespawnComponent;

	virtual void BeginPlay() override;

	virtual void OnPossess(APawn* InPawn) override;

	virtual void SetupInputComponent() override;

private:
	void OnPauseGame();

	void OnMatchStateChange(ESCMatchState State);

	void OnMuteSound();
	
};
