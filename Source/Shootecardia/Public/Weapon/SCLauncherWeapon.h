// Shootecardia Game. All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "Weapon/SCBaseWeapon.h"
#include "SCLauncherWeapon.generated.h"

class ASCProjectile;
class USoundCue;

UCLASS()
class SHOOTECARDIA_API ASCLauncherWeapon : public ASCBaseWeapon
{
	GENERATED_BODY()

public:
	virtual void StartFire() override;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Weapon")
	TSubclassOf<ASCProjectile> ProjectileClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Sound")
	USoundCue* NoAmmoSound;

	virtual void MakeShot() override;
};
