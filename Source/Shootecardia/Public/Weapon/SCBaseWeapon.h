// Shootecardia Game. All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SCCoreTypes.h"
#include "SCBaseWeapon.generated.h"

class USkeletalMeshComponent;
class APlayerController;
class UNiagaraSystem;
class UNiagaraComponent;
class USoundCue;

UCLASS()
class SHOOTECARDIA_API ASCBaseWeapon : public AActor
{
	GENERATED_BODY()

public:
	ASCBaseWeapon();

	virtual void StartFire();
	virtual void StopFire();

	FOnClipEmptySignature OnClipEmpty;

	void ChangeClip();

	bool CanReload();

	FWeaponUIData GetUIData() const { return UIData; }

	FAmmoData GetAmmoData() const { return CurrentAmmo; }

	bool TryToAddAmmo(int32 ClipsAmount);

	bool IsAmmoEmpty() const;

	bool IsAmmoFull();

	bool IsFiring() const;

	virtual void Zoom(bool Enabled)
	{
	}

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category="Components")
	USkeletalMeshComponent* WeaponMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FName MuzzleSocketName = "MuzzleSocket";

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Weapon")
	float TraceMaxDistance = 1500.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Weapon")
	FAmmoData DefaultAmmo{15, 10, false};

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="UI")
	FWeaponUIData UIData;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="VFX")
	UNiagaraSystem* MuzzleFX;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	bool FireInProgress;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Sound")
	USoundCue* FireSound;

	virtual void BeginPlay() override;

	virtual void MakeShot();

	bool GetPlayerViewPoint(FVector& ViewLocation, FRotator& ViewRotation) const;

	FVector GetMuzzleWorldLocation() const;

	virtual bool GetTraceData(FVector& TraceStart, FVector& TraceEnd) const;

	void MakeHit(FHitResult& HitResult, const FVector& TraceStart, const FVector& TraceEnd);

	void DecreaseAmmo();

	bool IsClipEmpty() const;

	void LogAmmo();

	UNiagaraComponent* SpawnMuzzleFX();

private:
	FTimerHandle ShotTimerHandle;

	FAmmoData CurrentAmmo;
};
