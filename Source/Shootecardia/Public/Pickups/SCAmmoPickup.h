// Shootecardia Game. All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "Pickups/SCBasePickup.h"
#include "SCAmmoPickup.generated.h"

class ASCBaseWeapon;

UCLASS()
class SHOOTECARDIA_API ASCAmmoPickup : public ASCBasePickup
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Pickup", meta = (ClampMin="1.0", ClampMax="10.0"))
	int32 ClipsAmount = 10;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Pickup")
	TSubclassOf<ASCBaseWeapon> WeaponType;

private:
	virtual bool GivePickupTo(APawn* PlayerPawn) override;
};
