// Shootecardia Game. All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SCBasePickup.generated.h"

class USphereComponent;
class USoundCue;

UCLASS()
class SHOOTECARDIA_API ASCBasePickup : public AActor
{
	GENERATED_BODY()

public:
	ASCBasePickup();

protected:
	UPROPERTY(VisibleAnywhere, Category="Pickup")
	USphereComponent* CollisionComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Pickup")
	float RespawnTime = 5.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Pickup")
	USoundCue* PickupTakenSound;

	virtual void BeginPlay() override;

	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

	virtual void NotifyActorEndOverlap(AActor* OtherActor) override;

public:
	virtual void Tick(float DeltaTime) override;

	bool CouldBeTaken() const;

private:
	UPROPERTY()
	TArray<APawn*> OverlappingPawns;

	float RotationYaw = 0.0f;

	void PickupWasTaken();

	void Respawn();

	virtual bool GivePickupTo(APawn* PlayerPawn);

	void GenerateRotationYaw();

	FTimerHandle RespawnTimerHandle;
};
