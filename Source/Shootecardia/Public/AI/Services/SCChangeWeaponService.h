// Shootecardia Game. All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "SCChangeWeaponService.generated.h"

UCLASS()
class SHOOTECARDIA_API USCChangeWeaponService : public UBTService
{
	GENERATED_BODY()

public:
	USCChangeWeaponService();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="AI", meta=(ClampMin = "0.0", ClampMax = "1.0"))
	float Probability = 0.5;

	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
	
};
