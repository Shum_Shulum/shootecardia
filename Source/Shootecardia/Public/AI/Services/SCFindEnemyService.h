// Shootecardia Game. All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "SCFindEnemyService.generated.h"

// Сервис поиска врага
UCLASS()
class SHOOTECARDIA_API USCFindEnemyService : public UBTService
{
	GENERATED_BODY()

public:
	USCFindEnemyService();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="AI")
	FBlackboardKeySelector EnemyActorKey;

	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
	
	
};
