// Shootecardia Game. All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "SCFireService.generated.h"

// сервис стрельбы НПС
UCLASS()
class SHOOTECARDIA_API USCFireService : public UBTService
{
	GENERATED_BODY()

public:
	USCFireService();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="AI")
	FBlackboardKeySelector EnemyActorKey;

	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
	
};
