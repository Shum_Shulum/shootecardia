// Shootecardia Game. All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "Player/SCBaseCharacter.h"
#include "SCAICharacter.generated.h"

class UBehaviorTree;
class UWidgetComponent;

UCLASS()
class SHOOTECARDIA_API ASCAICharacter : public ASCBaseCharacter
{
	GENERATED_BODY()

public:
	ASCAICharacter(const FObjectInitializer& ObjInit);

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="AI")
	UBehaviorTree* BehaviorTreeAsset;

	virtual void Tick(float DeltaSeconds) override;

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category="Components")
	UWidgetComponent* HealthWidgetComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="AI")
	float HealthVisibleDistance = 1000.0f;

	virtual void BeginPlay() override;

	virtual void OnHealthChanged(float Health, float HealthDelta) override;

	virtual void OnDeath() override;

private:
	void UpdateHealthWidgetVisibility();
};
