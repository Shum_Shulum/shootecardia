// Shootecardia Game. All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTDecorator.h"
#include "SCNeedAmmoDecorator.generated.h"

class ASCBaseWeapon;

UCLASS()
class SHOOTECARDIA_API USCNeedAmmoDecorator : public UBTDecorator
{
	GENERATED_BODY()

public:
	USCNeedAmmoDecorator();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="AI")
	TSubclassOf<ASCBaseWeapon> WeaponType;

	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;
};
