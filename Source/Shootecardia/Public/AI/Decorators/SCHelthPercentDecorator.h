// Shootecardia Game. All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTDecorator.h"
#include "SCHelthPercentDecorator.generated.h"

UCLASS()
class SHOOTECARDIA_API USCHelthPercentDecorator : public UBTDecorator
{
	GENERATED_BODY()

public:
	USCHelthPercentDecorator();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="AI")
	float HealthPercent = 0.6f;

	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;
};
