// Shootecardia Game. All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "SCAIController.generated.h"

class USCAIPerceptionComponent;
class USCRespawnComponent;

UCLASS()
class SHOOTECARDIA_API ASCAIController : public AAIController
{
	GENERATED_BODY()

public:
	ASCAIController();

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category="Components")
	USCAIPerceptionComponent* SCAIPerceptionComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category="Components")
	USCRespawnComponent* RespawnComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="AI")
	FName FocusOnKeyName = "EnemyActor";

	virtual void OnPossess(APawn* InPawn) override;

	virtual void Tick(float DeltaSeconds) override;

private:
	AActor* GetFocusOnActor() const;
};
