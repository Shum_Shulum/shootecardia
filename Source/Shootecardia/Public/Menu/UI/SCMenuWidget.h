// Shootecardia Game. All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SCCoreTypes.h"
#include "UI/SCBaseWidget.h"
#include "SCMenuWidget.generated.h"

class UButton;
class UHorizontalBox;
class USCGameInstance;
class USCLevelItemWidget;
class USoundCue;

UCLASS()
class SHOOTECARDIA_API USCMenuWidget : public USCBaseWidget
{
	GENERATED_BODY()

protected:
	UPROPERTY(meta=(BindWidget))
	UButton* StartGameButton;

	UPROPERTY(meta=(BindWidget))
	UButton* QuitGameButton;

	UPROPERTY(meta=(BindWidget))
	UHorizontalBox* LevelItemsBox;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite,Category="UI")
	TSubclassOf<UUserWidget> LevelItemsWidgetClass;

	UPROPERTY(meta=(BindWidgetAnim), Transient)
	UWidgetAnimation* HideAnimation;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite,Category="Sound")
	USoundCue* StartGameSound;
	
	virtual void NativeOnInitialized() override;

	virtual void OnAnimationFinished_Implementation(const UWidgetAnimation* Animation) override;

private:
	UPROPERTY()
	TArray<USCLevelItemWidget*> LevelItemWidgets;
	
	UFUNCTION()
	void OnStartGame();
	
	UFUNCTION()
	void OnQuitGame();

	void InitLevelItems();

	void OnLevelSelected(const FLevelData& Data);

	USCGameInstance* GetSCGameInstance() const;
};
