// Shootecardia Game. All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SCMenuGameModeBase.generated.h"

UCLASS()
class SHOOTECARDIA_API ASCMenuGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASCMenuGameModeBase();
	
};
