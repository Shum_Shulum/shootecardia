// Shootecardia Game. All Rights Reserved

using UnrealBuildTool;
using System.Collections.Generic;

public class ShootecardiaTarget : TargetRules
{
	public ShootecardiaTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "Shootecardia" } );
	}
}
